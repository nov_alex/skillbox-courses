import java.util.*;
import java.util.stream.DoubleStream;
import org.apache.commons.lang3.StringUtils;

public class Main {
    public static void main(String[] args) {

        String rainbow = "Каждый охотник желает знать, где сидит фазан";
        String[] colors = rainbow.split(",?\\s+");

        System.out.println("Прямой массив:");
        for (String color: colors) {
            System.out.println(color);
        }

        System.out.println("\nПеревёрнутый массив:");
        for (int i = 0; i < colors.length / 2; i++) {
            String temp = colors[i];
            colors[i] = colors[colors.length - i - 1];
            colors[colors.length - i - 1] = temp;
        }

        for (String color: colors) {
            System.out.println(color);
        }

        System.out.println("\nСредняя температура по больнице:");
        double[] temperatureDouble = new double[30];
        String[] temperatureStr = new String[30];

        int healthyCount = 0;
        for(int i = 0; i < temperatureDouble.length; i++) {
            double y = (double)Math.round(Math.random() * 100d) / 100d;
            temperatureDouble[i] = (double)Math.round(((y * 8) + 32) * 10d) / 10d;
            temperatureStr[i] = String.valueOf(temperatureDouble[i]);
            if (temperatureDouble[i] >= 36.2 && temperatureDouble[i] <= 36.9) {
                healthyCount++;
            }
        }

        String allTemp = String.join(", ", temperatureStr);
        System.out.println("Все температуры: " + allTemp);
        double middleTemp = DoubleStream.of(temperatureDouble).sum()/30;
        System.out.println("Средняя температура: " + (double)Math.round(middleTemp * 10d) / 10d);
        System.out.println("Количество здоровых пациентов: " + healthyCount);
        System.out.println();


        int height = getNumberFromConsole("Введите число от 3 и больше:");

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < height; j++) {
                if (j == i || j == (height - 1 - i)) {
                    System.out.print("X");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    private static int getNumberFromConsole(String inputMessage)
    {
        Integer result = null;
        do {
            System.out.println(inputMessage);
            Scanner scanner = new Scanner(System.in);
            String inputData = scanner.nextLine().strip();

            if (inputData.equals("")) {
                System.out.println("Нет данных, попробуйте ещё раз.");
            }
            try {
                result = Integer.parseInt(inputData);
            } catch (NumberFormatException e) {
                System.out.println("Результатом ввода должно быть число, попробуйте ещё раз.");
            }

        } while (null == result);

        return result;
    }
}
