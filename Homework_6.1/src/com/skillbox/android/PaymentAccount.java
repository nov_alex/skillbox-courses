package com.skillbox.android;

public class PaymentAccount
{
    protected double moneyAmount = 0.0;

    public PaymentAccount(double moneyAmount)
    {
        this.moneyAmount = moneyAmount;
        String accountName = this.getClass().getSimpleName().replace("Account", "").toLowerCase();
        System.out.println(String.format("Your %s account was successfully created. There's %.2f on your balance", accountName, moneyAmount));
    }

    public double getMoneyAmount()
    {
        return moneyAmount;
    }

    public void putMoney(double amount)
    {
        moneyAmount += amount;
        System.out.println(String.format("You successfully have put %.2f$! Your account balance is: %.2f$", amount, moneyAmount));
    }

    public void withdrawMoney(double amount)
    {
        if (this.isValid(amount)) {
            moneyAmount -= amount;
            System.out.println(String.format("You successfully have withdrawn %.2f$! Your account balance is: %.2f$", amount, moneyAmount));
        }
    }

    protected boolean isValid(double amount)
    {
        if (amount < 0) {
            System.out.println("Amount have to be higher than 0.");
            return false;
        }

        if (amount > moneyAmount) {
            System.out.println("Not enough money on your account! Please enter a lower amount!");
            return false;
        }

        return true;
    }
}
