package com.skillbox.android;

import java.util.*;

public class Main {

    public static void main(String[] args)
    {
        double amount = 1000;
        System.out.println("\nCreate deposit account with amount: " + amount);
        DepositAccount depositAccount = new DepositAccount(amount);
        assertEquals(depositAccount.getMoneyAmount(), amount);

        double payment = 100;
        System.out.println("\nPut 100$ to deposit account.");
        depositAccount.putMoney(payment);
        assertEquals(depositAccount.getMoneyAmount(), amount + payment);

        System.out.println("\nTry to withdraw 100$ from deposit account.");
        System.out.println("1. Now");
        depositAccount.withdrawMoney(100);
        assertEquals(depositAccount.getMoneyAmount(), amount + payment);

        System.out.println("2. In 20 days");
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.add( Calendar.DAY_OF_YEAR, -20);
        Date someDaysAgo = calendar.getTime();

        depositAccount.setLastDepositDate(someDaysAgo);
        depositAccount.withdrawMoney(100);
        assertEquals(depositAccount.getMoneyAmount(), amount + payment);

        System.out.println("3. In 32 days");
        calendar = GregorianCalendar.getInstance();
        calendar.add( Calendar.DAY_OF_YEAR, -32);
        someDaysAgo = calendar.getTime();

        depositAccount.setLastDepositDate(someDaysAgo);
        depositAccount.withdrawMoney(100);
        assertEquals(depositAccount.getMoneyAmount(), amount);

        System.out.println("\nTry to withdraw -2$ from deposit account.");
        depositAccount.withdrawMoney(-2);
        assertEquals(depositAccount.getMoneyAmount(), amount);


        System.out.println("\nCreate card account with amount: " + amount);
        CardAccount cardAccount = new CardAccount(amount);
        assertEquals(cardAccount.getMoneyAmount(), amount);

        System.out.println("\nPut 100$ to card account.");
        cardAccount.putMoney(payment);
        assertEquals(cardAccount.getMoneyAmount(), 1100);

        System.out.println("\nTry to withdraw 100$ from card account.");
        cardAccount.withdrawMoney(100);
        assertEquals(cardAccount.getMoneyAmount(), 999);

        System.out.println("\nTry to withdraw other 999$ from card account.");
        cardAccount.withdrawMoney(999);
        assertEquals(cardAccount.getMoneyAmount(), 999);
    }

    private static void assertEquals(double actual, double expected)
    {
        if (actual == expected) {
            System.out.println("OK");
        } else {
            System.out.println("FAIL");
        }
    }


}
