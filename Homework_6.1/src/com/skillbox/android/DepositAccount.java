package com.skillbox.android;

import java.util.Date;

public class DepositAccount extends PaymentAccount
{
    private static final int DEPOSIT_DAYS = 28;
    private Date lastDepositDate;

    public DepositAccount(double moneyAmount)
    {
        super(moneyAmount);
        lastDepositDate = new Date();
    }

    public void putMoney(double amount)
    {
        super.putMoney(amount);
        lastDepositDate = new Date();
    }

    public void withdrawMoney(double amount)
    {
        long currentTime = new Date().getTime();
        long lastDepositTime = lastDepositDate.getTime();

        long diff = currentTime - lastDepositTime;

        int day_count = (int)((float)diff / (24 * 60 * 60 * 1000));

        if (day_count > DEPOSIT_DAYS) {
            super.withdrawMoney(amount);
        } else {
            System.out.println(String.format("You can not withdraw money from your deposit account now.\n" +
                    "Only %d days have passed since your last enrollment.", day_count));
        }
    }

    public void setLastDepositDate(Date lastDepositDate)
    {
        this.lastDepositDate = lastDepositDate;
    }
}
