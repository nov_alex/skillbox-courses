package com.skillbox.android.bank;

import com.skillbox.android.bank.client.*;

public class Main {

    public static void main(String[] args) throws Exception
    {
        double amount = 1000;
        // Individual
        System.out.println("\nCreate individual client with amount: " + amount);
        Individual individual = new Individual();
        individual.setPaymentAmount(amount);
        assertEquals(individual.getPaymentAmount(), amount);

        double payment = 100;
        System.out.println("\nPut 100$ to individual client account.");
        individual.putMoney(payment);
        assertEquals(individual.getPaymentAmount(), amount + payment);

        System.out.println("\nTry to withdraw 100$ from individual client account.");
        individual.withdrawMoney(100);
        assertEquals(individual.getPaymentAmount(), amount);

        System.out.println("\nTry to withdraw -2$ from individual client account.");
        individual.withdrawMoney(-2);
        assertEquals(individual.getPaymentAmount(), amount);

        // Entity
        System.out.println("\nCreate entity client with amount: " + amount);
        Entity entity = new Entity();
        entity.setPaymentAmount(amount);
        assertEquals(entity.getPaymentAmount(), amount);

        System.out.println("\nTry to withdraw 100$ from entity client account.");
        entity.withdrawMoney(100);
        assertEquals(entity.getPaymentAmount(), amount - 100 - 1);

        System.out.println("\nTry to withdraw -2$ from entity client account.");
        entity.withdrawMoney(-2);
        assertEquals(entity.getPaymentAmount(), amount - 100 - 1);

        // Entrepreneur
        System.out.println("\nCreate entrepreneur client with amount: " + amount);
        Entrepreneur entrepreneur = new Entrepreneur();
        entrepreneur.setPaymentAmount(amount);
        assertEquals(entrepreneur.getPaymentAmount(), amount);

        System.out.println("\nPut 100$ to entrepreneur client account.");
        entrepreneur.putMoney(100);
        assertEquals(entrepreneur.getPaymentAmount(), amount + 100 - 1);

        System.out.println("\nPut 1100$ to entrepreneur client account.");
        entrepreneur.putMoney(1100);
        assertEquals(entrepreneur.getPaymentAmount(), 2193.5);

        System.out.println("\nTry to put -2$ to entrepreneur client account.");
        entrepreneur.putMoney(-2);
        assertEquals(entrepreneur.getPaymentAmount(), 2193.5);
    }

    private static void assertEquals(double actual, double expected)
    {
        if (actual == expected) {
            System.out.println("OK");
        } else {
            System.out.println("FAIL");
        }
    }
}
