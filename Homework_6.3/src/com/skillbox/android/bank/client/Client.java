package com.skillbox.android.bank.client;

import java.util.Map;
import java.util.TreeMap;

public abstract class Client
{
    private double paymentAccount = 0.0;
    private Map<Integer, Double> percentMap = new TreeMap<>();

    public double getPaymentAmount()
    {
        return paymentAccount;
    }

    public void setPaymentAmount(double paymentAccount)
    {
        this.paymentAccount = paymentAccount;
    }

    public void putMoney(double amount) throws Exception
    {
        if (amount > 0) {
            paymentAccount += amount;
            System.out.println(String.format("You successfully have put %.2f$! Your account balance is: %.2f$", amount, paymentAccount));
        } else {
            System.out.println("Amount have to be higher than 0.");
        }
    }

    public void withdrawMoney(double amount) throws Exception
    {
        if (amount > 0) {
            if (amount <= paymentAccount) {
                paymentAccount -= amount;
                System.out.println(String.format("You successfully have withdrawn %.2f$! Your account balance is: %.2f$", amount, paymentAccount));
            } else {
                System.out.println("Not enough money on your account! Please enter a lower amount!");
            }
        } else {
            System.out.println("Amount have to be higher than 0.");
        }
    }

    protected Map<Integer, Double> getPercentMap()
    {
        return percentMap;
    }


    protected double getPercent(double amount) throws Exception
    {
        if (percentMap.isEmpty()) {
            throw new Exception("Percent map is empty. You have to set percent map first.");
        }

        double result = 0.0;
        for (Map.Entry<Integer, Double> entry: percentMap.entrySet()) {
            if (amount >= entry.getKey()) {
                result = entry.getValue();
            } else {
                break;
            }
        }

        return result;
    }
}
