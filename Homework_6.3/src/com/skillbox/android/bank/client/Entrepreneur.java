package com.skillbox.android.bank.client;

import java.util.Map;

// ИП
public class Entrepreneur extends Client
{
    public Entrepreneur()
    {
        setPercentMap();
    }

    public void putMoney(double amount)
    {
        if (amount > 0) {
            try {
                double transferFee = amount / 100 * getPercent(amount);
                System.out.println(String.format("For putting money to the account, 1%% = %.2f$ of the putting amount will be charged.", transferFee));
                super.putMoney(amount - transferFee);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Amount have to be higher than 0.");
        }
    }

    private void setPercentMap()
    {
        getPercentMap().put(0, 1.0);
        getPercentMap().put(1000, 0.5);
    }
}
