import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    private static final String FILE_NAME = "src/sources/story.txt";

    public static void main(String[] args) {
        List<String> words = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] lineArray = line.split("\\s+");
                words.addAll(Arrays.asList(lineArray));
            }
        } catch (IOException e) {
            System.out.println("Ошибка: " + e.getMessage());
            e.printStackTrace();
        }
        words.removeIf(element -> (element.matches("[\\W\\d_]+")));

        for (int i = 0; i < words.size(); i++) {
            System.out.println((i+1) + ": " + words.get(i).replaceAll("[\\W\\d&&[^\\-`]]", ""));
        }
    }
}
