public class Main
{
    public static void main(String[] args) {
        var start1 = 200000;
        var end1 = 210000;
        var start2 = 220000;
        var end2 = 235000;

        // Вариант с использованием for
//        var count = 0;
//        for (int ticketNumber = start1; ticketNumber <= end1; ticketNumber++)
//        {
//            System.out.println(ticketNumber);
//            if ((ticketNumber == end1) && count == 0) {
//                ticketNumber = --start2;
//                end1 = end2;
//                count++;
//            }
//        }

        // Вариант с использованием while
        var ticketNumber = start1;
        var count = 0;
        while (ticketNumber <= end1)
        {
            System.out.println(ticketNumber);
            if ((ticketNumber == end1) && count == 0) {
                ticketNumber = --start2;
                end1 = end2;
                count++;
            }
            ticketNumber++;
        }
    }
}
