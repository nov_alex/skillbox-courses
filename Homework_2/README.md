### Lesson 1

Программа анализирует возможность и стоимость проезда автомобилей на основе их характеристик. <br>

----

### Lesson 2

[Код](/Lesson2/)

----

### Lesson 3

Booleans: [Код](/Lesson3/Booleans/)  <br>
Ages: [Код](/Lesson3/Ages/)

----

### Lesson 4

[Код](/Lesson4/TicketPrinter/)

----

### Lesson 5

RoadController: [Код](/Lesson3/RoadController/)  <br>
IncomeCalculator: [Код](/Lesson3/IncomeCalculator/)
