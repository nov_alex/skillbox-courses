import java.util.Scanner;

// Назначение программы: оценка расходов и возможности инвестиций исходя из месячного дохода компании
public class Main
{
    private static int minIncome;
    private static int maxIncome = 900000;

    private static int officeRentCharge = 140000;
    private static int telephonyCharge = 12000;
    private static int internetAccessCharge = 7200;

    private static int assistantSalary = 45000;
    private static int financeManagerSalary = 90000;

    private static double mainTaxPercent = 0.24;
    private static double managerPercent = 0.15;

    private static double minInvestmentsAmount = 100000;

    public static void main(String[] args)
    {
        // Считаем минимальный доход, при котором возможно инвестирование
        double minimalncome = (minInvestmentsAmount + calculateFixedCharges() * (1 - mainTaxPercent)) / (1 - mainTaxPercent) / (1 - managerPercent);

        // Округляем в большую сторону и переводим в int
        minIncome = (int) Math.ceil(minimalncome);

        // Запускаем бесконечный цикл
        while(true)
        {
            System.out.println("Введите сумму доходов компании за месяц " +
                "(от " + minIncome + " до 900 тысяч рублей): ");

            // предлагаем ввести расзмер месячного дохода компании
            int income = (new Scanner(System.in)).nextInt();

            // Если введённая сумма меньше нижнего лимита или больше верхнего лимита, прерываем текущцю итерацию цикла и идём на следующую, просим ввести сумму заново
            if(!checkIncomeRange(income)) {
                continue;
            }

            // Вычисляем зарплату менеджера
            double managerSalary = income * managerPercent;

            // Вычисляем чистый доход
            double pureIncome = income - managerSalary -
                calculateFixedCharges();

            // Вычисляем размер налога
            double taxAmount = mainTaxPercent * pureIncome;

            // Вычисляем размер чистого дохода после уплаты налогов
            double pureIncomeAfterTax = pureIncome - taxAmount;

            // Вычисляем возможность инвестирования
            boolean canMakeInvestments = pureIncomeAfterTax >=
                minInvestmentsAmount;

            System.out.println("Зарплата менеджера: " + managerSalary);
            System.out.println("Общая сумма налогов: " +
                (taxAmount > 0 ? taxAmount : 0));
            System.out.println("Компания может инвестировать: " +
                (canMakeInvestments ? "да" : "нет"));
            if(pureIncome < 0) {
                System.out.println("Бюджет в минусе! Нужно срочно зарабатывать!");
            }
        }
    }

    private static boolean checkIncomeRange(int income)
    {
        if(income < minIncome)
        {
            System.out.println("Доход меньше нижней границы");
            return false;
        }
        if(income > maxIncome)
        {
            System.out.println("Доход выше верхней границы");
            return false;
        }
        return true;
    }

    private static int calculateFixedCharges()
    {
        return officeRentCharge +
                telephonyCharge +
                internetAccessCharge +
                assistantSalary +
                financeManagerSalary;
    }
}
