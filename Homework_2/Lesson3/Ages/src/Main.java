public class Main
{
    public static void main(String[] args) {
        int vasyaAge = 60;
        int petyaAge = 40;
        int julyaAge = 7;

        int min = -1;
        int middle = -1;
        int max = -1;

        if (vasyaAge <= petyaAge && vasyaAge <= julyaAge) {
            min = vasyaAge;
            if (petyaAge <= julyaAge) {
                middle = petyaAge;
                max = julyaAge;
            } else {
                middle = julyaAge;
                max = petyaAge;
            }
        } else if (petyaAge <= vasyaAge && petyaAge <= julyaAge) {
            min = petyaAge;
            if (vasyaAge <= julyaAge) {
                middle = vasyaAge;
                max = julyaAge;
            } else {
                middle = julyaAge;
                max = vasyaAge;
            }
        } else {
            min = julyaAge;
            if (vasyaAge <= petyaAge) {
                middle = vasyaAge;
                max = petyaAge;
            } else {
                middle = petyaAge;
                max = vasyaAge;
            }
        }

        System.out.println("Минимальный возраст: " + min + " \nСредний возраст: "+ middle +" \nМаксимальный возраст: " +  max);
    }
}
