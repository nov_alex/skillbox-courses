public class Main
{
    public static void main(String[] args) {
        System.out.println("int:\nМинимальное значение: " + Integer.MIN_VALUE + ",\nМаксимальное значение: " + Integer.MAX_VALUE);
        System.out.println();
        System.out.println("byte:\nМинимальное значение: " + Byte.MIN_VALUE + ",\nМаксимальное значение: " + Byte.MAX_VALUE);
        System.out.println();
        System.out.println("short:\nМинимальное значение: " + Short.MIN_VALUE + ",\nМаксимальное значение: " + Short.MAX_VALUE);
        System.out.println();
        System.out.println("long:\nМинимальное значение: " + Long.MIN_VALUE + ",\nМаксимальное значение: " + Long.MAX_VALUE);
        System.out.println();
        System.out.println("float:\nМинимальное значение: " + Float.MIN_VALUE + ",\nМаксимальное значение: " + Float.MAX_VALUE);
        System.out.println();
        System.out.println("double:\nМинимальное значение: " + Double.MIN_VALUE + ",\nМаксимальное значение: " + Double.MAX_VALUE);
    }
}
