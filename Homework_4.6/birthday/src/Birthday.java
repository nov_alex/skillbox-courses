import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Birthday {
    public static void main(String[] args) throws ParseException {

        String dateTemplate = "^\\d{2}\\.\\d{2}\\.\\d{4}$";
        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

        Calendar currentDate = Calendar.getInstance();
        Calendar birthdayDate = null;
        do {
            System.out.println("Введите, пожалуйста, дату своего рождения:");
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine().strip();
            if (input.matches(dateTemplate)) {
                Calendar inputDate = Calendar.getInstance();
                inputDate.setTime(df.parse(input));
                if (inputDate.before(currentDate)) {
                    birthdayDate = inputDate;
                } else {
                    System.out.println("Указанная вами дата больше текущей, попробуйте ещё раз!");
                }
            } else {
                System.out.println("Неверный формат даты, попробуйте ещё раз!");
            }
        } while (null == birthdayDate);

        DateFormat day = new SimpleDateFormat("E");

        int count = 0;
        while (birthdayDate.before(currentDate)) {
            System.out.println(count + " - " + df.format(birthdayDate.getTime()) + " - " + day.format(birthdayDate.getTime()));
            birthdayDate.add(Calendar.YEAR, 1);
            count++;
        }
    }
}
