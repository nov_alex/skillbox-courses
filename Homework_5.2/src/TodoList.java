import java.util.ArrayList;
import java.util.Scanner;

public class TodoList {
    public static void main(String[] args) {
        ArrayList<String> todoList = new ArrayList<>();
        System.out.println("Список дел.\n\nВведите команду: ");

        String command = "";
        while (!command.equals("exit")){
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();

            if (input.isBlank()) {
                continue;
            }

            String[] inputParts = input.split("\\s+");
            command = inputParts[0];

            int index = -1;
            String thing = "";
            int secondWord = input.indexOf(" ") + 1;
            if (inputParts.length > 1) {
                if (inputParts[1].matches("[\\Dа-яА-Я]+")) {
                    thing = input.substring(secondWord);
                } else {
                    index = Integer.parseInt(inputParts[1]);
                    if (inputParts.length > 2) {
                        thing = input.substring(input.indexOf(" ", secondWord) + 1);
                    }
                }
            }

            switch (command.toLowerCase()) {
                case "list":
                    if (todoList.isEmpty()) {
                        System.out.println("Список дел:\n\t<- Здесь пока ещё нету дел ->");
                    } else {
                        todoList.forEach(item -> System.out.println(String.format("%d - %s", todoList.indexOf(item), item)));
                    }
                    break;
                case "add":
                    if (index == -1 || index > todoList.size()) {
                        todoList.add(thing);
                        System.out.println("Запись добавлена.");
                    } else {
                        todoList.add(index, thing);
                    }
                    break;
                case "edit":
                    if (index < todoList.size()) {
                        todoList.set(index, thing);
                        System.out.println("Запись отредактирована.");
                    } else {
                        System.out.println("Нет такого индекса в списке.");
                    }
                    break;
                case "delete":
                    if (index != -1 && index < todoList.size()) {
                        todoList.remove(index);
                        System.out.println(String.format("Запись с индексом %d удалена из списка", index));
                    } else {
                        System.out.println("Нет такого элемента в списке.");
                    }
                    break;
                case "exit":
                    break;
                default:
                    System.out.println(String.format("Нет такой команды: %s", inputParts[0]));
            }
            System.out.print("\nВведите команду: ");
        }
    }
}
