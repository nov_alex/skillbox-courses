public abstract class AbstractEmployee implements Employee
{
    private Company employeeCompany;

    public Company getEmployeeCompany()
    {
        return employeeCompany;
    }

    public void setEmployeeCompany(Company company)
    {
        employeeCompany = company;
    }

    public double getSalary()
    {
        return getPosition().getSalary();
    }

    protected boolean isEmployed()
    {
        Company company = getEmployeeCompany();
        if (null == company) {
            System.out.println("This person is unemployed");
            return false;
        }
        return true;
    }
}
