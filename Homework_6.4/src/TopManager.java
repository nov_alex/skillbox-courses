public class TopManager extends AbstractEmployee implements Employee
{
    public double getMonthSalary()
    {
        if (!isEmployed()) {
            return 0;
        }
        return getSalary() + getBonus();
    }

    private double getBonus()
    {
        Company company = getEmployeeCompany();
        if (null == company) {
            System.out.println("This person is unemployed");
            return 0;
        }
        return company.getIncome() > Company.INCOME_GOAL ? getSalary() * 1.5 : 0;
    }

    public Position getPosition()
    {
        return Position.TopManager;
    }
}
