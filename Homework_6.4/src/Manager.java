public class Manager extends AbstractEmployee implements Employee
{
    private double salesAmount;
    private static final double MONTH_SALES_PLAN = 100000;
    private static final int SALES_AMOUNT_PERCENT = 5;

    public Manager()
    {
        salesAmount = MONTH_SALES_PLAN + 300 * Math.random();
    }

    public double getMonthSalary()
    {
        if (!isEmployed()) {
            return 0;
        }
        return getSalary() + getPartOfSalesAmount(SALES_AMOUNT_PERCENT);
    }

    public double getPartOfSalesAmount(int percent)
    {
        return getSalesAmount() * percent / 100;
    }

    public double getSalesAmount()
    {
        return salesAmount;
    }

    public Position getPosition()
    {
        return Position.Manager;
    }
}
