import org.jetbrains.annotations.Contract;

import java.lang.reflect.Array;

public enum CatColor {
    WHITE ("Белый"),
    BLACK ("Чёрный"),
    GINGER ("Рыжий"),
    BLUE ("Голубой"),
    GREY ("Серый"),
    CREAM ("Кремовый"),
    BROWN ("Коричневый");

    private String title;

    @Contract(pure = true)
    CatColor(String title)
    {
        this.title = title;
    }

    @Override
    public String toString()
    {
        return title;
    }
}