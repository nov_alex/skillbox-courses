import java.util.Arrays;

public class Loader
{
    public static void main(String[] args)
    {
        Cat[] cats = new Cat[5];

        System.out.println("Lesson 1");
        for (int i = 0; i < cats.length; i++) {
            cats[i] = new Cat();
            System.out.println(String.format("Вес кошки %d: %d г", i+1, cats[i].getWeight().intValue()));
        }
        System.out.println("Количество кошек: " + Cat.getCount());
        System.out.println();

        System.out.println(String.format(
                "Кошка 1 весила: %d г, кошку покормили, новый вес: %d г",
                cats[0].getWeight().intValue(),
                cats[0].feed(1000.0).getWeight().intValue())
        );
        System.out.println();

        Double amount = 1000.0;
        do {
            System.out.println(String.format("Кошка 2 весит %d, кошку покормили %d г корма,", cats[1].getWeight().intValue(), amount.intValue()));
            cats[1].feed(amount);
        } while (!cats[1].getStatus().equals("Exploded"));

        System.out.println(String.format("Кошка 2 взорвалась, когда вес увеличился до %d г", cats[1].getWeight().intValue()));
        System.out.println("Количество кошек: " + Cat.getCount());
        System.out.println();

        do {
            System.out.print(String.format("Кошка 3 весит %d, кошка мяукнула: ", cats[2].getWeight().intValue()));
            cats[2].meow();
        } while (!cats[2].getStatus().equals("Dead"));

        System.out.println(String.format("Кошка 3 погибла, когда её вес уменьшился до %d г", cats[2].getWeight().intValue()));
        System.out.println("Количество кошек: " + Cat.getCount());

        System.out.println("= = = = = = = = = = = = = = =\n");

        System.out.println("Lesson 2");
        System.out.println(String.format(
                "Кошка 3 весила: %d г, кошке дали 100 г корма, новый вес: %d г",
                cats[3].getWeight().intValue(),
                cats[3].feed(100.0).getWeight().intValue())
        );
        System.out.println(String.format(
                "Кошка 3 весила: %d г, кошке дали 150 г корма, новый вес: %d г",
                cats[3].getWeight().intValue(),
                cats[3].feed(150.0).getWeight().intValue())
        );
        System.out.println(String.format("Количество съеденной еды: %d г", cats[3].getEatenAmount().intValue()));
        System.out.println();

        System.out.print(String.format("Кошка 4 весила: %d г, кошка сходила в туалет: ", cats[4].getWeight().intValue()));
        cats[4].goToTheToilet();
        System.out.println(String.format("Вес кошки стал: %d", cats[4].getWeight().intValue()));

        System.out.println("= = = = = = = = = = = = = = =\n");

        System.out.println("Lesson 3");
        System.out.println("Количество кошек: " + Cat.getCount());
        System.out.println("= = = = = = = = = = = = = = =\n");

        System.out.println("Lesson 4");
        System.out.println("Количество глаз: " + Cat.EYES_COUNT);
        System.out.println("Минимальный вес: " + Cat.MIN_WEIGHT);
        System.out.println("Максимальный вес: " + Cat.MAX_WEIGHT);

        System.out.println("Окрасы кошек: " + Arrays.toString(CatColor.values()));
        System.out.println("= = = = = = = = = = = = = = =\n");

        System.out.println("Lesson 5");
        System.out.println("Сгенерировали кошку с весом: " + generateCat(2500.0).getWeight() + " г");
        System.out.println("Сгенерировали кошку с весом: " + generateCat(3700.0).getWeight() + " г");
        System.out.println("= = = = = = = = = = = = = = =\n");

        System.out.println("Lesson 6");
        cats[4].setColor(CatColor.BLACK);
        System.out.println("Цвет пятой кошки: " + cats[4].getColor().toString());
        System.out.println("= = = = = = = = = = = = = = =\n");

        System.out.println("Lesson 7");
        Cat catCopy = cats[4].copyCat();
        System.out.println("Вес оригинальной кошки: " + cats[4].getWeight().intValue());
        System.out.println("Вес копии кошки: " + catCopy.getWeight().intValue());
        cats[4].feed(500.0);
        System.out.println("Оригинальную кошку покормили, её вес стал: " + cats[4].getWeight().intValue());
        System.out.println("Вес копии кошки остался прежним: " + catCopy.getWeight().intValue());
    }

    private static Cat generateCat(double weight)
    {
        return new Cat(weight);
    }
}