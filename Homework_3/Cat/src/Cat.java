public class Cat
{
    public static final int EYES_COUNT = 2;
    public static final double MIN_WEIGHT = 1000.0;
    public static final double MAX_WEIGHT = 9000.0;
    protected static final String DEAD_STATUS = "This cat has already been dead!";

    private double originWeight;
    private double weight;

    private double eatenAmount;

    private static int count;
    private CatColor color;

    public Cat()
    {
        weight = 1500.0 + 3000.0 * Math.random();
        originWeight = weight;

        count++;
    }

    public Cat(double weight)
    {

        this.weight = weight;
        originWeight = weight;

        count++;
    }

    public void meow()
    {
        if (this.isAlive()) {
            weight = weight - 300;
            System.out.println("Meow");
            refreshCatsCount();
        }
    }

    public Cat feed(Double amount)
    {
        if (this.isAlive()) {
            eatenAmount += amount;
            weight = weight + amount;

            refreshCatsCount();
        }

        return this;
    }

    public Double getEatenAmount()
    {
        return eatenAmount;
    }

    public void setEatenAmount(double eatenAmount)
    {
        this.eatenAmount = eatenAmount;
    }

    public void goToTheToilet()
    {
        if (this.isAlive()) {
            int amount = 300 + (int) (200 * Math.random());
            weight = weight - amount;
            System.out.println(String.format("I became %d gr lighter, want more food", amount));

            refreshCatsCount();
        }
    }

    public void drink(Double amount)
    {
        if (this.isAlive()) {
            weight = weight + amount;

            refreshCatsCount();
        }
    }

    public Double getWeight()
    {
        return weight;
    }

    public String getStatus()
    {
        if(weight < MIN_WEIGHT) {
            return "Dead";
        }
        else if(weight > MAX_WEIGHT) {
            return "Exploded";
        }
        else if(weight > originWeight) {
            return "Sleeping";
        }
        else {
            return "Playing";
        }
    }

    public void refreshCatsCount()
    {
        if (getStatus().equals("Dead") || getStatus().equals("Exploded")) {count--;}
    }

    private Boolean isAlive()
    {
        boolean status = getStatus().equals("Dead") || getStatus().equals("Exploded");
        if (status) {
            System.out.println(DEAD_STATUS);
        }
        return !status;
    }

    public static int getCount()
    {
        return count;
    }

    public void setColor(CatColor color)
    {
        this.color = color;
    }

    public CatColor getColor()
    {
        return color;
    }

    public Cat copyCat()
    {
        Cat cat = new Cat(weight);
        cat.setColor(color);
        cat.setEatenAmount(eatenAmount);

        return cat;
    }
}