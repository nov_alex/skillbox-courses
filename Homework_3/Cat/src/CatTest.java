import static org.junit.jupiter.api.Assertions.*;

class CatTest extends Cat {

    @org.junit.jupiter.api.Test
    void testMeow() {
        Cat cat1 = new Cat(MIN_WEIGHT);
        System.out.println(this.getWeight());
        cat1.meow();
        assertEquals(1, Cat.getCount());
        cat1.meow();
        assertEquals(1, Cat.getCount());
    }

    @org.junit.jupiter.api.Test
    void testFeed() {
        Cat cat1 = new Cat(MAX_WEIGHT);
        cat1.feed(200.0);
        assertEquals(1, Cat.getCount());
        cat1.feed(200.0);
        assertEquals(1, Cat.getCount());
    }

    @org.junit.jupiter.api.Test
    void testGoToTheToilet() {
        Cat cat1 = new Cat(MIN_WEIGHT);
        cat1.goToTheToilet();
        assertEquals(1, Cat.getCount());
        cat1.goToTheToilet();
        assertEquals(1, Cat.getCount());
    }

    @org.junit.jupiter.api.Test
    void testDrink() {
        Cat cat1 = new Cat(MAX_WEIGHT);
        cat1.drink(200.0);
        assertEquals(1, Cat.getCount());
        cat1.drink(200.0);
        assertEquals(1, Cat.getCount());
    }

    @org.junit.jupiter.api.Test
    void testCopyCat() {
        Cat cat = this.copyCat();
        assertEquals(cat.getWeight(), this.getWeight());
        assertEquals(cat.getEatenAmount(), this.getEatenAmount());
        assertEquals(cat.getColor(), this.getColor());
        assertNotEquals(cat, this);
    }
}