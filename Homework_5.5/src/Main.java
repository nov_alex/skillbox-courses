import java.util.*;

// Пример гос. номера: A111AA197
// Возможные буквы:    АВЕМНОРСТУ
// Возможные цифры:    1-9
// Коды региона:       1 - 197

public class Main {
    public static void main(String[] args) {
        ArrayList<String> carNumbersList = new ArrayList<>();
        Character[] letters = {'А', 'В', 'Е', 'М', 'Н', 'О', 'Р', 'С', 'Т', 'У'};
        String format = "%1$s%2$d%2$d%2$d%3$s%4$s%5$d";

        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 999; i++) {
            for (int j = 1; j <= 9; j++) {
                for (int k = 1; k <= 197; k++) {
                    String str = String.valueOf(i);
                    int firstRankIndex;
                    int secondRankIndex = 0;
                    int thirdRankIndex = 0;
                    if (1 == str.length()) {
                        firstRankIndex = i;
                    } else if (2 == str.length()){
                        firstRankIndex = i % 10;
                        secondRankIndex = i / 10 % 10;
                    } else {
                        firstRankIndex = i % 10;
                        secondRankIndex = i / 10 % 10;
                        thirdRankIndex = i / 100 % 10;
                    }
                    carNumbersList.add(String.format(format, letters[thirdRankIndex], j, letters[secondRankIndex], letters[firstRankIndex], k));
                }
            }
        }
        Collections.sort(carNumbersList);

        TreeSet<String> carNumbersTreeSet = new TreeSet<>(carNumbersList);
        HashSet<String> carNumbersHashSet = new HashSet<>(carNumbersList);
        String defaultNumber = "Р777СУ197";

        for (;;) {
            System.out.print(String.format("\nВведите гос номер [%s]: ", defaultNumber));
            String input = scanner.nextLine();

            if (input.isBlank()) {
                input = defaultNumber;
            }

            long start = System.currentTimeMillis();
            boolean resultBool = carNumbersList.contains(input);
            long duration = System.currentTimeMillis() - start;

            System.out.println(String.format(
                    "\nArrayList\nПрямой перебор.ёеРезультат: Номер%s найден. Продолжительность: %d мс",
                    (resultBool ? "" : " не"),
                    duration
            ));

            start = System.currentTimeMillis();
            int intResult = Collections.binarySearch(carNumbersList, input);
            duration = System.currentTimeMillis() - start;

            System.out.println(String.format(
                    "Бинарный поиск. Результат: %d. Продолжительность: %d мс",
                    intResult,
                    duration
            ));

            start = System.currentTimeMillis();
            resultBool = carNumbersTreeSet.contains(input);
            duration = System.currentTimeMillis() - start;

            System.out.println(String.format(
                    "\nTreeSet\nПрямой перебор. Результат: Номер%s найден. Продолжительность: %d мс",
                    (resultBool ? "" : " не"),
                    duration
            ));

            start = System.currentTimeMillis();
            resultBool = carNumbersHashSet.contains(input);
            duration = System.currentTimeMillis() - start;

            System.out.println(String.format(
                    "\nHashSet\nПрямой перебор. Результат: Номер%s найден. Продолжительность: %d мс",
                    (resultBool ? "" : " не"),
                    duration
            ));
        }
    }
}
