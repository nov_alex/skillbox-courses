import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Loader
{
    public static void main(String[] args)
    {
        for (char c = 'a'; c <= 'z'; c++) {
            System.out.println(String.format("Код символа %s: %s", c, (int) c));
        }
        for (char c = 'A'; c <= 'Z'; c++) {
            System.out.println(String.format("Код символа %s: %s", c, (int) c));
        }
        System.out.println();


        String text = "Вася заработал 5000 рублей, Петя - 7563 рубля, а Маша - 30000 рублей";

        int lastSpaceIndex = text.lastIndexOf(' ');
        List<Integer> numbers = new ArrayList<>();
        for (int a = 0; a <=lastSpaceIndex; a++) {
            for (int i = 0; i <= 9; i++) {
                if (text.charAt(a) == Character.forDigit(i, Character.MAX_RADIX)) {
                    numbers.add(Integer.parseInt(text.substring(a, text.indexOf(' ', a))));
                }
            }
            a = text.indexOf(' ', a);
        }

        int sum = 0;
        for (int num: numbers) {
            sum += num;
        }
        System.out.println(text);
        System.out.println(String.format("Общий заработок: %d рублей", sum));
        System.out.println();

        String[] fioMembers = {"Фамилия", "Имя", "Отчество"};

        String fio = "";
        do {
            System.out.println("Введите ФИО:");
            Scanner scanner = new Scanner(System.in);
            fio = scanner.nextLine().strip();
            if (fio.equals("")) {
                System.out.println("Нет данных, попробуйте ещё раз.");
            }
        } while (fio.equals(""));

        int lastWordStartIndex = fio.lastIndexOf(' ') + 1;
        List<String> words = new ArrayList<>();
        for (int a = 0; a <=lastWordStartIndex; a++) {
            if (a == lastWordStartIndex) {
                words.add(fio.substring(a));
                break;
            }
            words.add(fio.substring(a, fio.indexOf(' ', a)));
            a = fio.indexOf(' ', a);
        }

        for (int p = 0; p < fioMembers.length; p++) {
            if (p < words.size()) {
                System.out.println(String.format("%s: %s", fioMembers[p], words.get(p)));
            }
        }
    }
}