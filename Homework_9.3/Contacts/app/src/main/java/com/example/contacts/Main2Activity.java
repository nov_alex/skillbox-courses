package com.example.contacts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Main2Activity extends AppCompatActivity {

    EditText editName;
    EditText editSurname;
    EditText editPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        editName = findViewById(R.id.editName);
        editSurname = findViewById(R.id.editSurname);
        editPhone = findViewById(R.id.editPhone);

        Intent intentToFirstActivity = new Intent();
        intentToFirstActivity.putExtra("key", "Подтвердите данные!");
        setResult(RESULT_CANCELED, intentToFirstActivity);
    }

    public void onCloseActivity(View view) {
        Intent intentToFirstActivity_OK = new Intent();
        intentToFirstActivity_OK.putExtra("name", editName.getText().toString());
        intentToFirstActivity_OK.putExtra("surname", editSurname.getText().toString());
        intentToFirstActivity_OK.putExtra("phone", editPhone.getText().toString());
        setResult(RESULT_OK, intentToFirstActivity_OK);
        this.finish();
    }
}
