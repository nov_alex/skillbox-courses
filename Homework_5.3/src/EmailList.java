import java.util.Scanner;
import java.util.TreeSet;

public class EmailList {
    public static void main(String[] args) {
        TreeSet<String> emailList = new TreeSet<>();
        System.out.println("Email list.\n\nEnter a command: ");

        String command = "";
        while (!command.equals("exit")){
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();

            if (input.isBlank()) {
                continue;
            }

            String[] inputParts = input.split(",?\\s+");
            command = inputParts[0];

            TreeSet<String> validList = new TreeSet<>();
            String errors = "";
            if (inputParts.length > 1) {
                for (int i = 1; i < inputParts.length; i++) {
                    if (!inputParts[i].matches("^[\\w]+@[\\w]+\\.[a-zA-Z]+$")) {
                        errors = errors.isEmpty() ? inputParts[i] : errors.concat(", " + inputParts[i]);
                    } else {
                        validList.add(inputParts[i]);
                    }
                }
                if (!errors.isEmpty()) {
                    System.out.println(String.format("There are bad format emails: %s.", errors));
                }
            }

            switch (command.toLowerCase()) {
                case "list":
                    if (emailList.isEmpty()) {
                        System.out.println("Email list:\n\t<- There is no emails yet ->");
                    } else {
                        emailList.forEach(System.out::println);
                    }
                    break;
                case "add":
                    emailList.addAll(validList);
                    if (validList.size() > 1) {
                        System.out.println(String.format("%s emails have been added.", errors.isEmpty() ? "All" : "Other"));
                    } else {
                        System.out.println(String.format("%s has been added.", errors.isEmpty() ? "Email" : "Another email"));
                    }
                    break;
                case "exit":
                    break;
                default:
                    System.out.println(String.format("There is no command: %s", inputParts[0]));
            }
            System.out.print("\nEnter a command: ");
        }
    }
}
