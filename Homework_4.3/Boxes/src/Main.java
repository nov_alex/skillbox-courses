import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Введите количество ящиков:");
        Scanner scanner = new Scanner(System.in);

        int amountOfBoxes = scanner.nextInt();
        int amountOfContainers = getAmount(amountOfBoxes, Container.MAX_CAPACITY);
        int amountOfTrucks = getAmount(amountOfContainers, Truck.MAX_CAPACITY);

        System.out.println("amountOfBoxes: " + amountOfBoxes);
        System.out.println("amountOfContainers: " + amountOfContainers);
        System.out.println("amountOfTrucks: " + amountOfTrucks);

        Truck[] trucks = new Truck[amountOfTrucks];
        for (int t = 0; t < amountOfTrucks; t++) {
            trucks[t] = new Truck();

            int containersCount = Math.min(amountOfContainers, Truck.MAX_CAPACITY);
            Container[] containers = new Container[containersCount];
            for (int c = 0; c < containersCount;  c++) {
                containers[c] = new Container();

                int boxesCount = Math.min(amountOfBoxes, Container.MAX_CAPACITY);
                Box[] boxes = new Box[boxesCount];
                for (int b = 0; b < boxesCount; b++) {
                    boxes[b] = new Box();
                }
                containers[c].setBoxes(boxes);
                amountOfBoxes -= Container.MAX_CAPACITY;
            }
            trucks[t].setContainers(containers);
            amountOfContainers -= Truck.MAX_CAPACITY;
        }

        for (Truck truck: trucks) {
            System.out.println(truck.getName());
            for (Container container: truck.getContainers()) {
                System.out.println("\t" + container.getName());
                for (Box box: container.getBoxes()) {
                    System.out.println("\t\t" + box.getName());
                }
            }
        }
    }

    private static int getAmount(int amount, int maxCapacity) {
        int result = amount / maxCapacity;
        if (result > 0) {
            int modulo = amount % maxCapacity;
            if (modulo > 0) {
                result++;
            }
        } else {
            result = 1;
        }
        return result;
    }
}
