public class Box {
    private String name = "Box ";
    private static int count;

    public Box()
    {
        count++;
        name += count;
    }

    public String getName()
    {
        return name;
    }
}
