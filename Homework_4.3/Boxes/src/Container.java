public class Container {
    public static final int MAX_CAPACITY = 27;
    private String name = "Container ";
    private Box[] boxes;
    private static int count;

    public Container()
    {
        count++;
        name += count;
    }

    public String getName()
    {
        return name;
    }

    public Box[] getBoxes() {
        return boxes;
    }

    public void setBoxes(Box[] boxes) {
        this.boxes = boxes;
    }
}
